use heimdallr::Vault;
use icbiadb::{self, prelude::*, storage::BTreeMap, KvDb};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub struct Session<'a> {
    pub(crate) db: KvDb<BTreeMap>,
    pub(crate) vault_meta: Vec<VaultMetaInfo>,
    pub(crate) unlocked: HashMap<String, Vault<'a>>,
}

impl<'a> Default for Session<'a> {
    fn default() -> Self {
        let db = icbiadb::kv::create("heimdallr.icb").unwrap();
        let vault_meta = db
            .starts_with("vault:")
            .iter()
            .map(|(k, o)| o.extract::<VaultMetaInfo>())
            .collect();

        Self {
            db,
            vault_meta,
            unlocked: HashMap::new(),
        }
    }
}

impl<'a> Session<'a> {
    pub fn get_vault_list(&self) -> Vec<VaultMetaInfo> {
        self.vault_meta.to_vec()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct VaultMetaInfo {
    pub(crate) file_name: String,
    pub(crate) path: String,
    pub(crate) factors: u8,
    pub(crate) factor_types: [String; 2],
    pub(crate) unlocked: bool,
}

impl Default for VaultMetaInfo {
    fn default() -> Self {
        VaultMetaInfo {
            file_name: String::new(),
            path: String::new(),
            factors: 0,
            factor_types: [String::new(), String::new()],
            unlocked: false,
        }
    }
}

impl VaultMetaInfo {
    pub fn new(
        file_name: String,
        path: String,
        factors: u8,
        factor_types: [String; 2],
        unlocked: bool,
    ) -> Self {
        Self {
            file_name,
            path,
            factors,
            factor_types,
            unlocked,
        }
    }
}
