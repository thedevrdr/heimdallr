use crate::utils::*;
use crate::{AUTH_TYPE_STRING, USB_DRIVE_TEST_OPTIONS};
use heimdallr::SodiumArray;
use iced::{pick_list, text_input, Column, Element, PickList, Row, TextInput};

#[derive(Clone, Debug)]
pub enum Message {
    PickAuthTypeChanged(&'static str),
    PasswordInput(String),
    SelectDriveChanged(String),
}

#[derive(Clone)]
pub struct SelectAuth {
    pick_auth_type: u8,
    pick_auth_type_state: pick_list::State<&'static str>,
    auth_type: AuthWidget,
}

impl Default for SelectAuth {
    fn default() -> Self {
        Self {
            pick_auth_type: 1,
            pick_auth_type_state: Default::default(),
            auth_type: Default::default(),
        }
    }
}

impl SelectAuth {
    pub fn factor(&self) -> u8 {
        match self.auth_type {
            AuthWidget::Password(..) => 1,
            AuthWidget::UsbDrive(..) => 2,
            AuthWidget::Phone => 3,
        }
    }

    pub fn r#type(&self) -> &str {
        match self.auth_type {
            AuthWidget::Password(..) => "Password",
            AuthWidget::UsbDrive(..) => "Usb drive",
            AuthWidget::Phone => "Phone",
        }
    }

    pub fn value(&mut self) -> SodiumArray {
        match self.auth_type {
            AuthWidget::Password(_, ref mut value) => std::mem::take(value),
            AuthWidget::UsbDrive(_, ref mut value) => std::mem::take(value),
            _ => SodiumArray::new(String::new()).unwrap(),
        }
    }

    pub fn update(&mut self, message: Message) {
        match message {
            Message::PickAuthTypeChanged(value) => {
                if value == "Password" {
                    self.auth_type = AuthWidget::default();
                } else if value == "Usb drive" {
                    self.auth_type = AuthWidget::new_usb_drive_list();
                }

                self.pick_auth_type = factor_type_to_id(value)
            }
            _ => self.auth_type.update(message),
        }
    }

    pub fn view(&mut self) -> Element<Message> {
        Row::new()
            .spacing(5)
            .push(PickList::new(
                &mut self.pick_auth_type_state,
                AUTH_TYPE_STRING.to_owned(),
                Some(factor_type_id_to_str(self.pick_auth_type)),
                Message::PickAuthTypeChanged,
            ))
            .push(self.auth_type.view())
            .into()
    }
}

#[derive(Clone)]
pub enum AuthWidget {
    Password(text_input::State, SodiumArray),
    UsbDrive(pick_list::State<String>, SodiumArray),
    // TODO IMPLEMENT PHONE AUTHENTICATION
    Phone,
}

impl Default for AuthWidget {
    fn default() -> Self {
        AuthWidget::Password(
            text_input::State::default(),
            SodiumArray::new(String::new()).unwrap(),
        )
    }
}

impl AuthWidget {
    pub fn new_usb_drive_list() -> Self {
        AuthWidget::UsbDrive(
            pick_list::State::default(),
            SodiumArray::new(String::from("C:")).unwrap(),
        )
    }

    pub fn update(&mut self, message: Message) {
        match self {
            Self::Password(_, ref mut value, ..) => match message {
                Message::PasswordInput(new_text) => {
                    *value = SodiumArray::new(new_text).unwrap();
                }
                _ => {}
            },
            Self::UsbDrive(_, ref mut value) => match message {
                Message::SelectDriveChanged(new_drive) => {
                    *value = SodiumArray::new(String::from(new_drive)).unwrap();
                }
                _ => {}
            },
            _ => {}
        }
    }

    pub fn view(&mut self) -> Element<Message> {
        match self {
            AuthWidget::Password(state, value) => {
                // TODO, look for solution
                value.read_unlock();
                let data = value.as_str().to_string();
                value.lock();
                return TextInput::new(state, "Password", &data, Message::PasswordInput)
                    .padding(7)
                    .size(17)
                    .password()
                    .into();
            }
            AuthWidget::UsbDrive(state, value) => {
                // TODO, look for solution
                value.read_unlock();
                let data = value.as_str().to_string();
                value.lock();
                return PickList::new(
                    state,
                    USB_DRIVE_TEST_OPTIONS.to_vec(),
                    Some(data),
                    Message::SelectDriveChanged,
                )
                .padding(7)
                .into();
            }
            _ => {}
        }

        Column::new().into()
    }
}
