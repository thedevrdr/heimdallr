use std::path::Path;

use crate::{common::VaultMetaInfo, theme, Message};
use iced::{
    button, container::Style, scrollable, Align, Button, Color, Column, Container, Element,
    HorizontalAlignment, Length, Row, Scrollable, Text,
};
use iced_aw::native::IconText;

// TODO
// Only use VaultListWidgetMessage
#[derive(Clone, Debug, PartialEq)]
pub enum VaultListItemWidgetMessage {
    RemoveVaultListing,
    LockVault,
    UnlockVault,
    Test,
}

#[derive(Clone)]
pub struct VaultListItemWidget {
    pub(crate) vault_meta: VaultMetaInfo,
    lock_button_state: button::State,
    remove_button_state: button::State,
}

impl VaultListItemWidget {
    pub fn new(vault_meta: VaultMetaInfo) -> Self {
        Self {
            vault_meta,
            lock_button_state: Default::default(),
            remove_button_state: Default::default(),
        }
    }

    pub fn view(&mut self) -> Element<VaultListItemWidgetMessage> {
        let path = Path::new(&self.vault_meta.path).join(&self.vault_meta.file_name);
        let file_name = std::path::Path::new(&self.vault_meta.file_name);
        let col = Column::new().padding(30).push(
            Row::new()
                .align_items(Align::Center)
                .push(
                    Row::new()
                        .spacing(5)
                        .width(Length::FillPortion(4))
                        .align_items(Align::Center)
                        .push(
                            Row::new()
                                .spacing(25)
                                // Display vaults meta data
                                .push::<Element<VaultListItemWidgetMessage>>(if path.exists() {
                                    // Show lock/unlocked status
                                    if !self.vault_meta.unlocked {
                                        Button::new(
                                            &mut self.lock_button_state,
                                            Column::new()
                                                .align_items(Align::Center)
                                                .push(Text::new("Unlock").size(12))
                                                .push(
                                                    IconText::new("\u{f4d9}")
                                                        .color(Color::from_rgb(0.0, 0.0, 1.0))
                                                        .size(60),
                                                ),
                                        )
                                        .style(theme::VaultIconButton)
                                        .on_press(VaultListItemWidgetMessage::UnlockVault)
                                        .into()
                                    } else {
                                        Button::new(
                                            &mut self.lock_button_state,
                                            Column::new()
                                                .align_items(Align::Center)
                                                .push(Text::new("Lock").size(12))
                                                .push(
                                                    IconText::new("\u{f4de}")
                                                        .color(Color::from_rgb(1.0, 0.49, 0.0))
                                                        .size(60),
                                                ),
                                        )
                                        .style(theme::VaultIconButton)
                                        .on_press(VaultListItemWidgetMessage::LockVault)
                                        .into()
                                    }
                                } else {
                                    // Show File-not-found error
                                    Column::new()
                                        .spacing(3)
                                        .align_items(Align::Center)
                                        .push(Text::new("File not found!").size(12))
                                        .push(
                                            IconText::new("\u{f301}")
                                                .color(Color::from_rgb(1.0, 0.0, 0.0))
                                                .size(60),
                                        )
                                        .into()
                                })
                                // Show file name and meta
                                .push(
                                    Column::new()
                                        .push({
                                            let mut row = Row::new().push(
                                                Text::new(
                                                    path.file_stem().unwrap().to_string_lossy(),
                                                )
                                                .size(25),
                                            );

                                            if let Some(ext) = path.extension() {
                                                row = row.push(
                                                    Text::new(format!(
                                                        ".{}",
                                                        ext.to_string_lossy()
                                                    ))
                                                    .size(12),
                                                );
                                            }

                                            row
                                        })
                                        .push(
                                            Text::new(format!(
                                                "{}{}",
                                                self.vault_meta.factor_types[0],
                                                if self.vault_meta.factors == 2 {
                                                    format!(", {}", self.vault_meta.factor_types[1])
                                                } else {
                                                    "".to_string()
                                                }
                                            ))
                                            .width(Length::FillPortion(6))
                                            .size(14),
                                        )
                                        .push(Text::new("local").size(14)),
                                ),
                        ),
                )
                // Remove vault button
                .push(
                    Column::new()
                        .align_items(Align::End)
                        .width(Length::FillPortion(1))
                        .push(
                            Button::new(&mut self.remove_button_state, Text::new("X"))
                                .on_press(VaultListItemWidgetMessage::RemoveVaultListing),
                        ),
                ),
        );

        Container::new(col)
            .style(Dwad)
            .height(Length::Shrink)
            .into()
    }
}

#[derive(Clone, Debug)]
pub enum VaultListWidgetMessage {
    VaultListItemWidgetMessage(usize, VaultListItemWidgetMessage),
    UnlockedVault(VaultMetaInfo),
    Test,
}

#[derive(Clone, Default)]
pub struct VaultListWidget {
    pub(crate) vaults: Vec<VaultListItemWidget>,
    pub(crate) scroll: scrollable::State,
}

impl VaultListWidget {
    pub fn new(vaults: Vec<VaultListItemWidget>) -> Self {
        Self {
            vaults,
            scroll: scrollable::State::default(),
        }
    }

    pub fn update(&mut self, message: VaultListWidgetMessage) -> Option<Message> {
        None
    }

    pub fn view(&mut self) -> Element<VaultListWidgetMessage> {
        let vault_list = if !self.vaults.is_empty() {
            self.vaults
                .iter_mut()
                .enumerate()
                .fold(Column::new().spacing(7), |c, (i, e)| {
                    c.push(
                        e.view().map(move |message| {
                            VaultListWidgetMessage::VaultListItemWidgetMessage(i, message)
                        }), //.map(VaultListWidgetMessage::VaultListItemWidgetMessage),
                    )
                })
        } else {
            Column::new()
                .width(Length::Fill)
                .align_items(Align::Center)
                .push(Text::new("No vaults added"))
        };

        Scrollable::new(&mut self.scroll)
            .padding(20)
            .width(Length::Fill)
            .height(Length::Shrink)
            .push(Container::new(vault_list).width(Length::Fill))
            .into()
    }
}

pub struct Dwad;

impl iced::container::StyleSheet for Dwad {
    fn style(&self) -> Style {
        Style {
            border_width: 2.0,
            border_color: Color::from_rgb(0.7, 0.7, 0.7),
            ..Default::default()
        }
    }
}
