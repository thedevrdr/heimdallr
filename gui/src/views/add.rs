use std::path::Path;

use crate::{
    common::{Session, VaultMetaInfo},
    utils::*,
    widgets::authenticate_select,
    ViewTrait,
};
use iced::{
    button, pick_list, text_input, Align, Button, Column, Container, Element, Length, PickList,
    Row, Text, TextInput,
};
use iced_aw::{TabLabel, Tabs};

#[derive(Clone, Debug)]
pub enum AddViewMessage {
    None,
    Home,
    TabSelected(usize),
    CreateVault {
        name: String,
        path: String,
        factors: [(u8, String); 2],
    },
    CreateVaultViewMessage(CreateVaultViewMessage),
}

pub struct AddView {
    active_tab: usize,
    create_vault_view: CreateVaultView,
}

impl Default for AddView {
    fn default() -> Self {
        AddView {
            active_tab: 0,
            create_vault_view: Default::default(),
        }
    }
}

impl AddView {
    pub fn factors(&self) -> [u8; 2] {
        [
            self.create_vault_view.select_auth_widget_1.factor(),
            self.create_vault_view.select_auth_widget_2.factor(),
        ]
    }
}

impl ViewTrait for AddView {
    type Message = AddViewMessage;

    fn update(&mut self, session: &mut Session, message: AddViewMessage) -> Option<crate::Message> {
        match message {
            AddViewMessage::TabSelected(value) => self.active_tab = value,
            AddViewMessage::CreateVaultViewMessage(message) => {
                if let CreateVaultViewMessage::CancelAddViewVault = message {
                    return Some(crate::Message::ChangeToHomeView);
                }

                match self.create_vault_view.update(session, message) {
                    CreateVaultViewMessage::Home => return Some(crate::Message::ChangeToHomeView),
                    _ => {}
                }
            }
            _ => {}
        }

        None
    }

    fn view(&mut self, _session: &mut Session) -> Element<AddViewMessage> {
        let tabs = Tabs::new(self.active_tab, AddViewMessage::TabSelected)
            .tab_bar_position(iced_aw::TabBarPosition::Top)
            .push(
                // Create vault
                TabLabel::Text(String::from("Create")),
                Container::new(
                    self.create_vault_view
                        .view()
                        .map(AddViewMessage::CreateVaultViewMessage),
                )
                .padding(60),
            )
            .push(
                // Add local
                TabLabel::Text(String::from("Add local")),
                Container::new(Text::new("Hello world!")),
            )
            .push(
                // Add remote
                TabLabel::Text(String::from("Add remote")),
                Container::new(Text::new("Hello world!")),
            );

        tabs.into()
    }
}

#[derive(Clone, Debug)]
pub enum CreateVaultViewMessage {
    None,
    Home,
    CreateVault,
    CancelAddViewVault,
    CreateNameInputChanged(String),
    CreatePathInputChanged(String),
    CreateFactorsChanged(u8),
    AuthTypeMessage1(authenticate_select::Message),
    AuthTypeMessage2(authenticate_select::Message),
}

pub struct CreateVaultView {
    create_button_state: button::State,
    cancel_button_state: button::State,
    create_vault_name: String,
    create_vault_name_state: text_input::State,
    create_vault_path: String,
    create_vault_path_state: text_input::State,
    create_vault_factors: u8,
    create_vault_factors_state: pick_list::State<u8>,
    select_auth_widget_1: authenticate_select::SelectAuth,
    select_auth_widget_2: authenticate_select::SelectAuth,
}

impl Default for CreateVaultView {
    fn default() -> Self {
        Self {
            create_button_state: Default::default(),
            cancel_button_state: Default::default(),
            create_vault_name: String::new(),
            create_vault_name_state: Default::default(),
            create_vault_path: std::env::current_dir()
                .unwrap()
                .to_string_lossy()
                .to_string(),
            create_vault_path_state: Default::default(),
            create_vault_factors: 2,
            create_vault_factors_state: Default::default(),
            select_auth_widget_1: authenticate_select::SelectAuth::default(),
            select_auth_widget_2: authenticate_select::SelectAuth::default(),
        }
    }
}

impl CreateVaultView {
    pub fn update(
        &mut self,
        session: &mut Session,
        message: CreateVaultViewMessage,
    ) -> CreateVaultViewMessage {
        match message {
            CreateVaultViewMessage::CreateVault => {
                let authenticators = [
                    (
                        self.select_auth_widget_1.factor(),
                        self.select_auth_widget_1.value(),
                    ),
                    (
                        self.select_auth_widget_2.factor(),
                        self.select_auth_widget_2.value(),
                    ),
                ];

                let mut path = Path::new(&self.create_vault_path).join(&self.create_vault_name);
                path = if path.is_relative() {
                    Path::new(&std::env::current_dir().unwrap()).join(path)
                } else {
                    path
                };

                // TODO, handle
                let vault = create_vault(path, authenticators);

                let vault_meta = VaultMetaInfo::new(
                    self.create_vault_name.to_string(),
                    self.create_vault_path.to_string(),
                    self.create_vault_factors,
                    [
                        self.select_auth_widget_1.r#type().to_string(),
                        if self.create_vault_factors == 2 {
                            self.select_auth_widget_2.r#type().to_string()
                        } else {
                            "".to_string()
                        },
                    ],
                    false,
                );

                session
                    .db
                    .set(format!("vault:{}", self.create_vault_name), &vault_meta);
                // TODO, handle
                session.db.commit().unwrap();
                session.vault_meta.push(vault_meta);

                vault.save().unwrap();

                return CreateVaultViewMessage::Home;
            }
            CreateVaultViewMessage::CreateNameInputChanged(value) => {
                self.create_vault_name = if !value.ends_with(".hmd") {
                    value + ".hmd"
                } else {
                    value
                };
            }
            CreateVaultViewMessage::CreatePathInputChanged(value) => {
                self.create_vault_path = value;
            }
            CreateVaultViewMessage::CreateFactorsChanged(factor) => {
                self.create_vault_factors = factor;
            }
            CreateVaultViewMessage::AuthTypeMessage1(message) => {
                self.select_auth_widget_1.update(message)
            }
            CreateVaultViewMessage::AuthTypeMessage2(message) => {
                self.select_auth_widget_2.update(message)
            }
            _ => {}
        }

        CreateVaultViewMessage::None
    }

    pub fn view(&mut self) -> Element<CreateVaultViewMessage> {
        Column::new()
            .spacing(10)
            .push(
                TextInput::new(
                    &mut self.create_vault_name_state,
                    "Vault name",
                    &self.create_vault_name,
                    CreateVaultViewMessage::CreateNameInputChanged,
                )
                .padding(7)
                .size(17),
            )
            .push(
                TextInput::new(
                    &mut self.create_vault_path_state,
                    "Path",
                    &self.create_vault_path,
                    CreateVaultViewMessage::CreatePathInputChanged,
                )
                .padding(7)
                .size(17),
            )
            .push(
                Row::new()
                    .spacing(15)
                    .push(Text::new("Factors"))
                    .push(PickList::new(
                        &mut self.create_vault_factors_state,
                        vec![1, 2],
                        Some(self.create_vault_factors),
                        CreateVaultViewMessage::CreateFactorsChanged,
                    )),
            )
            .push({
                let mut c = Column::new().spacing(15).padding(10);

                if self.create_vault_factors > 0 {
                    c = c.push(
                        self.select_auth_widget_1
                            .view()
                            .map(CreateVaultViewMessage::AuthTypeMessage1),
                    )
                }

                if self.create_vault_factors == 2 {
                    c = c.push(
                        self.select_auth_widget_2
                            .view()
                            .map(CreateVaultViewMessage::AuthTypeMessage2),
                    )
                }
                c
            })
            .push(
                Row::new()
                    // TODO
                    // Place buttons on right side
                    .width(Length::Fill)
                    .align_items(Align::End)
                    .padding(10)
                    .spacing(5)
                    .push(
                        Button::new(&mut self.cancel_button_state, Text::new("Cancel"))
                            .on_press(CreateVaultViewMessage::CancelAddViewVault),
                    )
                    .push(
                        Button::new(&mut self.create_button_state, Text::new("Create"))
                            .on_press(CreateVaultViewMessage::CreateVault),
                    ),
            )
            .into()
    }
}
