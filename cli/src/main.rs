use heimdallr::{AuthType, Entry, SodiumString, Vault};
use std::path::PathBuf;

pub fn add_entries(vault: &mut Vault, parent: &str, count: usize) {
    dbg!("Adding {} entries", count);
    for x in 0..count {
        let entry = Entry::Login {
            title: format!("test{}.com", x),
            username: String::from("Test1"),
            password: String::from("Test2"),
        };
        vault.new_entry(Some(parent), entry);
    }
}

pub fn list_entries(vault: &Vault, group: &str) {
    dbg!("Listing {}", group);
    if let Some(root) = vault.get_group(group) {
        for uuid in root.iter() {
            let entry = vault.get_entry(uuid);
            dbg!("{:?}", entry);
        }
    }
}

pub fn remove_entries(vault: &mut Vault, group: &str, count: usize) {
    dbg!("Removing {} entries", count);
    if let Some(children) = vault.get_group(group) {
        if children.len() >= count {
            for uuid in children.iter().take(count) {
                vault.delete_entry(Some(group), &uuid)
            }
        } else {
            for key in children.iter() {
                vault.delete_entry(Some(group), key)
            }
        }
    }
}

fn main() {
    heimdallr::init();

    let path = PathBuf::from("test.hmd");
    let psw = AuthType::new_password(SodiumString::new(String::from("testpassw")).unwrap());
    let psw2 = AuthType::new_password(SodiumString::new(String::from("testpassw2")).unwrap());

    if path.exists() {
        if let Ok(mut vault) = Vault::open(path) {
            dbg!("Init vault, factors: {}", vault.factors());
            match vault.unlock(&[psw, psw2]) {
                Ok(_) => {
                    dbg!("unlocked vault");

                    list_entries(&vault, "test_group");
                    //remove_entries(&mut vault, "root", 1);
                    //vault.add_group(None, String::from("test_group"));
                    //add_entries(&mut vault, "test_group", 2);

                    vault.save().unwrap();
                }
                Err(e) => eprintln!("Error {:?}", e),
            }
        }
    } else if let Ok(mut vault) = Vault::create(path, &[psw, psw2]) {
        add_entries(&mut vault, "root", 2);
        vault.save().unwrap();
    }
}
