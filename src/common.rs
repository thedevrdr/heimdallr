use std::{ffi::c_void, ops::AddAssign};

use argon2::Argon2;
use libsodium_sys::{
    sodium_allocarray, sodium_free, sodium_mprotect_noaccess, sodium_mprotect_readonly,
};

pub struct SecureString(*mut c_void, usize);

impl SecureString {
    pub fn new(data: String) -> Result<Self, String> {
        let len = data.len();
        let ptr = unsafe {
            let ptr = sodium_allocarray(len, std::mem::size_of::<u8>());
            if ptr.is_null() {
                return Err(String::from("Failed initialization"));
            }
            let slice = std::slice::from_raw_parts_mut(ptr as *mut u8, len);
            slice.copy_from_slice(data.as_bytes());
            sodium_mprotect_noaccess(ptr);
            ptr
        };

        Ok(SecureString(ptr, len))
    }

    pub fn lock(&self) {
        unsafe { sodium_mprotect_noaccess(self.0) };
    }

    pub fn read_unlock(&self) {
        unsafe { sodium_mprotect_readonly(self.0) };
    }

    pub fn as_str<'a>(&self) -> &'a str {
        unsafe {
            let slice = std::slice::from_raw_parts(self.0 as *mut u8, self.1);
            std::str::from_utf8_unchecked(slice)
        }
    }
}

impl Drop for SecureString {
    fn drop(&mut self) {
        unsafe { sodium_free(self.0) };
    }
}

impl AddAssign for SecureString {
    fn add_assign(&mut self, rhs: Self) {
        *self = SecureString::new(String::from(self.as_str().to_owned() + rhs.as_str())).unwrap();
    }
}

pub struct PasswordString {
    data: SecureString
}

impl PasswordString {
    pub fn new(phc: String, data: SecureString) -> Self {
        PasswordString {
            data,
        }
    }
}
