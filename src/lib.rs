#![allow(unused_variables, unused_imports, dead_code)]

pub mod cipher;
pub mod database;
pub mod error;
pub mod sodium;
pub mod usb;
pub mod vault;

pub use argon2::password_hash::SaltString;
use libsodium_sys::sodium_init;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
pub use sodium::SodiumArray;
pub use vault::Vault;
use zeroize::Zeroize;

pub const AG2_PARAMS: argon2::Params = argon2::Params {
    output_size: 64,
    t_cost: 10,
    m_cost: 65536,
    p_cost: 4,
    version: argon2::Version::V0x13,
};

pub enum AuthType {
    Password(SodiumArray),
    UsbKey(SodiumArray),
    None,
}

impl AuthType {
    pub fn new_password(password: SodiumArray) -> Self {
        AuthType::Password(password)
    }

    pub fn new_usb_key(uuid: SodiumArray) -> Self {
        AuthType::UsbKey(uuid)
    }

    pub fn to_u8(&self) -> u8 {
        match self {
            AuthType::None => 0,
            AuthType::Password(_) => 1,
            AuthType::UsbKey(_) => 2,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, Zeroize, PartialEq)]
pub enum Entry {
    Login {
        title: String,
        username: String,
        password: String,
    },
    Website {
        title: String,
        username: String,
        password: String,
        url: String,
    },
    File {
        title: String,
        data: Vec<u8>,
    },
    Wallet {
        coin: String,
        encryption_key: String,
        seed_phrase: String,
    },
    None,
}

pub fn init() {
    unsafe {
        if sodium_init() == -1 {
            panic!("Failed to initialize sodium")
        }
    }
}

pub struct EncryptedString {
    data: Vec<u8>,
}
