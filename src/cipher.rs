use crate::SodiumArray;
use aes_gcm::aead::{Aead, NewAead};
use aes_gcm::{Aes256Gcm, Key, Nonce};
use argon2::{password_hash::SaltString, Argon2, Params, PasswordHasher, Version, ARGON2ID_IDENT};
use rand::{rngs::OsRng, Rng, RngCore};
use serde::{Deserialize, Serialize};
use sha3::{Digest, Sha3_512};
use std::iter::repeat;
use zeroize::Zeroize;

pub fn create_salt(size: usize) -> String {
    rand::thread_rng()
        .sample_iter(&rand::distributions::Alphanumeric)
        .take(size)
        .map(char::from)
        .collect()
}

pub fn create_iv(size: usize) -> Vec<u8> {
    let mut random: Vec<u8> = repeat(0u8).take(size).collect();
    OsRng.fill_bytes(&mut random);
    random
}

#[derive(Serialize, Deserialize)]
pub struct StringCipher {
    key: SodiumArray,
    iv: Vec<u8>,
}

impl StringCipher {
    pub fn new() -> Self {
        let params = Params {
            output_size: 32,
            ..Params::default()
        };
        let password = create_salt(32);
        let iv = create_iv(12);
        let salt = SaltString::new(&create_salt(32)).unwrap();
        let argon2 = argon2::Argon2::new(None, 10, 65536, 4, Version::V0x13).unwrap();
        let key = argon2
            .hash_password(password.as_bytes(), Some(ARGON2ID_IDENT), params, &salt)
            .unwrap();

        let hash = key.hash.unwrap().as_bytes().to_vec();

        Self {
            key: hash.into(),
            iv,
        }
    }

    pub fn encrypt(&self, plain: SodiumArray) -> SodiumArray {
        let nonce = Nonce::from_slice(&self.iv);
        self.key.read_unlock();
        let cipher = Aes256Gcm::new(Key::from_slice(self.key.inner()));
        self.key.lock();
        // TODO, handle result
        let result = cipher.encrypt(nonce, plain.to_string().as_bytes()).unwrap();
        result.into()
    }

    pub fn decrypt<'a>(&self, encrypted: SodiumArray) -> SodiumArray {
        let nonce = Nonce::from_slice(&self.iv);
        self.key.read_unlock();
        let cipher = Aes256Gcm::new(Key::from_slice(self.key.inner()));
        self.key.lock();
        // TODO, handle result
        let result = cipher
            .decrypt(nonce, encrypted.to_string().as_bytes())
            .unwrap();
        result.into()
    }
}

pub struct Cipher<'a> {
    pub(crate) key: Vec<u8>,
    pub(crate) salt_key_pairs: Vec<(SaltString, Vec<u8>, String)>,
    pub(crate) iv: Vec<u8>,
    pub(crate) argon2: Argon2<'a>,
}

impl<'a> Cipher<'a> {
    pub fn new(secure_key: &SodiumArray, salt: &String, iv: &[u8]) -> Self {
        let argon2 = argon2::Argon2::new(None, 10, 65536, 4, Version::V0x13).unwrap();

        let mut salt_key_pairs = Vec::with_capacity(10);

        secure_key.read_unlock();
        let password = secure_key.as_str();
        let handles: Vec<std::thread::JoinHandle<(SaltString, Vec<u8>, String)>> = (0..11)
            .map(|i| {
                let argon2 = argon2.clone();
                let salt = salt.clone();

                std::thread::spawn(move || {
                    let params = Params {
                        output_size: 64,
                        ..Params::default()
                    };

                    if i == 0 {
                        let salt = SaltString::new(&salt).unwrap();
                        let key = argon2
                            .hash_password(password.as_bytes(), Some(ARGON2ID_IDENT), params, &salt)
                            .unwrap();
                        let phc = key.to_string();
                        let hash = key.hash.unwrap().as_bytes().to_vec();
                        (salt, hash, phc)
                    } else {
                        let plain = create_salt(32);
                        let salt = SaltString::new(&plain).unwrap();
                        let key = argon2
                            .hash_password(password.as_bytes(), Some(ARGON2ID_IDENT), params, &salt)
                            .unwrap();
                        let phc = key.to_string();
                        let hash = key.hash.unwrap().as_bytes().to_vec();
                        (salt, hash, phc)
                    }
                })
            })
            .collect();

        for handle in handles {
            salt_key_pairs.push(handle.join().unwrap());
        }
        secure_key.lock();
        let key = salt_key_pairs.remove(0).1;

        Cipher {
            key,
            salt_key_pairs,
            iv: iv.to_vec(),
            argon2,
        }
    }

    fn encrypt_bytes(
        &self,
        plain: &[u8],
        key: &[u8],
        iv: &[u8],
    ) -> Result<Vec<u8>, aes_gcm::Error> {
        let nonce = Nonce::from_slice(iv);
        let cipher = Aes256Gcm::new(Key::from_slice(key));
        cipher.encrypt(nonce, plain)
    }

    fn decrypt_bytes(
        &self,
        encrypted: &[u8],
        key: &[u8],
        iv: &[u8],
    ) -> Result<Vec<u8>, aes_gcm::Error> {
        let nonce = Nonce::from_slice(iv);
        let cipher = Aes256Gcm::new(Key::from_slice(key));
        cipher.decrypt(nonce, encrypted)
    }

    pub fn encrypt(&self, data: &[u8]) -> Result<(Vec<u8>, String, Vec<u8>), aes_gcm::Error> {
        let iv = create_iv(12);

        let idx = { OsRng.gen_range(0..10) };
        let salt_key_pair = &self.salt_key_pairs[idx];

        // The first 32 bytes is the key for hash encryption.
        let hash_encryption_key: Vec<u8> = salt_key_pair.1.iter().take(32).cloned().collect();
        // The second 32 bytes is the key for data encryption.
        let data_encryption_key: Vec<u8> =
            salt_key_pair.1.iter().skip(32).take(32).cloned().collect();
        //dbg!("{:?}", &data_encryption_key);

        // Encrypt data
        let encrypted_data_bytes = self.encrypt_bytes(data, &data_encryption_key, &iv)?;

        // Calculate hash and encrypt
        //let hash_bytes = Sha3_512::digest(&encrypted_data_bytes);
        //let encrypted_hash_bytes = self.encrypt_bytes(&hash_bytes, &hash_encryption_key, &iv)?;

        //Ok(encrypted_data_bytes)
        Ok((
            encrypted_data_bytes,
            salt_key_pair.0.as_str().to_string(),
            iv,
            //encrypted_hash_bytes,
        ))
    }

    pub fn decrypt(&self, data: &[u8]) -> Result<Vec<u8>, aes_gcm::Error> {
        // The first 32 bytes of the key is for hash decryption.
        let hash_decryption_key: Vec<u8> = self.key.iter().take(32).cloned().collect();
        // The second 32 bytes is the key for data decryption.
        let data_decryption_key: Vec<u8> = self.key.iter().skip(32).take(32).cloned().collect();

        //let hash = self.decrypt_bytes(&hash, &hash_decryption_key, &iv)?;
        //let integrity_check_ok = self.hasher.validate_hash(&bytes_to_decrypt, &hash);
        let final_result = self.decrypt_bytes(&data, &data_decryption_key, &self.iv)?;
        //(final_result, integrity_check_ok)
        Ok(final_result)
    }
}
