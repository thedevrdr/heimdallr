use argon2::Argon2;
use libsodium_sys::{
    sodium_allocarray, sodium_free, sodium_malloc, sodium_mprotect_noaccess,
    sodium_mprotect_readonly,
};
use serde::{Deserialize, Serialize};
use std::{ffi::c_void, ops::AddAssign};
use zeroize::Zeroize;

use crate::database::Database;

#[derive(Clone)]
pub struct SodiumArray(*mut c_void, usize);

impl SodiumArray {
    pub fn new(mut data: String) -> Result<Self, String> {
        let len = data.len();
        let ptr = unsafe {
            let ptr = sodium_allocarray(len, std::mem::size_of::<u8>());
            if ptr.is_null() {
                return Err(String::from("Failed initialization"));
            }
            std::slice::from_raw_parts_mut(ptr as *mut u8, len).copy_from_slice(data.as_bytes());
            sodium_mprotect_noaccess(ptr);
            ptr
        };
        data.zeroize();

        Ok(SodiumArray(ptr, len))
    }

    pub fn lock(&self) {
        unsafe { sodium_mprotect_noaccess(self.0) };
    }

    pub fn read_unlock(&self) {
        unsafe { sodium_mprotect_readonly(self.0) };
    }

    pub fn inner<'a>(&self) -> &'a [u8] {
        unsafe { std::slice::from_raw_parts(self.0 as *mut u8, self.1) }
    }

    pub fn as_str<'a>(&self) -> &'a str {
        unsafe {
            let slice = std::slice::from_raw_parts(self.0 as *mut u8, self.1);
            std::str::from_utf8_unchecked(slice)
        }
    }

    pub fn to_string(&self) -> String {
        self.read_unlock();
        let string = unsafe {
            let slice = std::slice::from_raw_parts(self.0 as *mut u8, self.1);
            std::str::from_utf8_unchecked(slice).to_string()
        };
        self.lock();
        string
    }
}

impl From<Vec<u8>> for SodiumArray {
    fn from(mut data: Vec<u8>) -> Self {
        let len = data.len();
        let ptr = unsafe {
            let ptr = sodium_allocarray(len, std::mem::size_of::<u8>());
            if ptr.is_null() {
                panic!("{}", "Failed initialization");
            }
            std::slice::from_raw_parts_mut(ptr as *mut u8, len).copy_from_slice(&data);
            sodium_mprotect_noaccess(ptr);
            ptr
        };
        data.zeroize();

        SodiumArray(ptr, len)
    }
}

impl From<String> for SodiumArray {
    fn from(mut data: String) -> Self {
        let len = data.len();
        let ptr = unsafe {
            let ptr = sodium_allocarray(len, std::mem::size_of::<u8>());
            if ptr.is_null() {
                panic!("{}", "Failed initialization");
            }
            std::slice::from_raw_parts_mut(ptr as *mut u8, len).copy_from_slice(data.as_bytes());
            sodium_mprotect_noaccess(ptr);
            ptr
        };
        data.zeroize();

        SodiumArray(ptr, len)
    }
}

impl std::default::Default for SodiumArray {
    fn default() -> Self {
        Self::new(String::from("")).unwrap()
    }
}

impl Drop for SodiumArray {
    fn drop(&mut self) {
        unsafe { sodium_free(self.0) };
    }
}

impl std::fmt::Debug for SodiumArray {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str("**SODIUM PROTECTED STRING**")
    }
}

impl AddAssign for SodiumArray {
    fn add_assign(&mut self, rhs: Self) {
        *self = SodiumArray::new(String::from(self.as_str().to_owned() + rhs.as_str())).unwrap();
    }
}

unsafe impl std::marker::Send for SodiumArray {}

impl Serialize for SodiumArray {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.read_unlock();
        let result = serde::Serialize::serialize(self.as_str(), serializer);
        self.lock();
        result
    }
}

impl<'de> Deserialize<'de> for SodiumArray {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Vec::deserialize(deserializer).map(|data| data.into())
    }
}

#[test]
fn serialize_deserialize() {
    let mut db = icbiadb::kv::mem::<icbiadb::storage::BTreeMap>();
    let test: SodiumArray = "Hello World!".to_string().into();
    db.set("ss", test);
    let test2 = db.get_value::<SodiumArray>("ss");
    test2.read_unlock();
    assert!(test2.as_str() == "Hello World!");
    test2.lock();
}
