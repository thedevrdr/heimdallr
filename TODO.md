
Saftey steps:

- [ ] Application require USB-drive/master passw to unlock file
- [ ] Database is stored in libsodium allocated locked memory
- [ ] Passwords-declared strings are session-based encrypted while in memory
- [ ] Input form selection and send string on windows/x11 removing the need for copy-paste


**Secure database in memory**

Read encrypted data in unsafe memory
Decrypt and write data to safe memory
Alloc safe memory for database and initialize/parse from safe memory data
Sensitive serialized data deserializes to SodiumArray and stays encrypted

https://docs.rs/placement-new/0.3.0/placement_new/

 **TOTP**

https://github.com/buttercup/buttercup-desktop/issues/594

**USB-drive key**

https://github.com/iqlusioninc/yubikey.rs

* Saftey checks to validate the true drive
* Change encryption/hashing for each unlock
* In-database logging of access


# Useable links for development
Enclaves:
Intel SGX & Amd SEV enclaves

https://docs.microsoft.com/en-us/windows/win32/api/enclaveapi/nf-enclaveapi-createenclave

https://github.com/openenclave/openenclave

https://github.com/fortanix/rust-sgx

https://security.stackexchange.com/questions/175749/what-are-the-functional-similarities-and-differences-between-tpm-and-sgx-in-trus


**Argon2id + AES**

https://crypto.stackexchange.com/questions/75021/encrypt-a-file-with-aes-using-a-secret-key-derived-with-argon2
https://github.com/rust-keylock/rust-keylock-lib/blob/master/src/datacrypt.rs#L107
