
# Heimdallr

**Under development, not ready for use yet**

Yet another vault-based password manager. Heimdallr maybe doesn't bring much new to the table and is mostly developed for my personal use and education.


**Features**

- [x] 1/2FA
- [ ] Authentication types, master password, USB drive-key, phone auth
- [ ] File and WebDAV storage options
- [x] AesGCM254 + Argon2ID encrypted password storage
- [x] Libsodium secure memory management on runtime


# Compiling user-interfaces

```
cargo build -p [heimdallr-gui, heimdallr-cli]
```

```
cargo run -p heimdallr-gui
```

# How it works

* When creating a vault, a master key is generated directly after selecting authenticate methods and then stored in protected memory
* Inserting an entry into the database in-memory, encrypts any password-classed string while all other fields are not encrypted
* When the file is written, everything is encrypted again with the same master key, keeping only number of auth factors, auth factor types, salt and iv in clear view
* The generated key from the authentication types(password, usb drive or phone) is kept in a libsodium protected memory sector while the vault is unlocked


# Credit

The encryption code(cipher.rs) is mostly based on a rewritten version of [rust-keyring](https://github.com/rust-keylock) encryption https://github.com/rust-keylock/rust-keylock-lib/blob/master/src/datacrypt.rs
